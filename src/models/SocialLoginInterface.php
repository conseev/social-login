<?php

interface SocialLoginInterface
{
	public static function doLogin($user, $network);
	
	public function doUpdate(Array $user_data);

	public static function doRegister(Array $user_data);
}
