<?php

class SocialLoginIdentity extends Eloquent
{
	public $fillable = array(
		'global_id', 'user_id', 'email',
		'first_name', 'last_name', 'avatar',
		'token', 'token_expires_at', 'type',
	);

	public function systemUser()
	{
		$class = SocialLogin::getUserClass();
		return $this->belongsTo($class, 'user_id');
	}

	public static function getIdentities($user = null)
	{
		if (empty($user)) {
			$user = Auth::user();
			if (empty($user)) {
				throw new \RuntimeException("No user");
			}
		}

		$identities = array();
		$services   = Config::get('social_login');
		$multipass  = \MultiPass\Configuration::getInstance();

		foreach (self::where('user_id', $user->id)->get() as $identity) {
			$strategy = $multipass->getStrategy(ucfirst($identity->type));
			switch($identity->type) {
			case 'facebook':
				$options = $strategy->options;
				$client = new Facebook(array(
					'appId' => $options['client_id'],
					'secret' => $options['client_secret'],
				));
				$client->setAccessToken($identity->token);
				break;
			default:
				$oauth2 = $strategy->getClient();
				$client = \OAuth2\AccessToken::fromHash($oauth2, array('access_token' => $identity->token));
				break;
			}

			$identities[$identity->type]      = $client;
			$identities[$identity->global_id] = $client;
		}

		return $identities;
	}

	public static function getIdentity($type, $user = null)
	{
		$identities = self::getIdentities($user);
		if (empty($identities[$type])) {
			throw new \RuntimeException("Cannot find identity $type for user");
		}
		return $identities[$type];
	}

	public static function register($user_data)
	{
		$class = SocialLogin::getUserClass();
		if ($user = $class::where('email', $user_data['email'])->first()) {
			$user->doUpdate($user_data);
			return $user;
		}

		return $class::doRegister($user_data);
	}

	public function login($network)
	{
		$class = SocialLogin::getUserClass();
		return $class::doLogin($this->getUser(), $network);
	}

	public function getUser()
	{
		$class = SocialLogin::getUserClass();
		return $class::where('id', $this->user_id)->first();
	}
}
