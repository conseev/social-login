<?php

class SocialLogin
{
	protected static $class;

	public static function getServices($only_missing = false, $user = null)
	{
		$urls = array();
		$services = Config::get('social_login');
		$accounts = array();
		if ($only_missing) {
			try {
				$accounts = SocialLoginIdentity::getIdentities($user);
			} catch (\Exception $e) {}
		}

		foreach ($services as $name => $config) {
			if (!is_array($config)) continue;
			if (!empty($accounts[strtolower($name)])) continue;
			$urls[URL::route('sl_login', array('type' => $name))] = $name;
		}

		return $urls;
	}

	public static function getUserClass()
	{
		return self::$class;
	}

	public static function setUserClass($user_class)
	{
		if (!class_exists($user_class)) {
			throw new \RuntimeException("Cannot find class $user_class");
		}

		if (!is_subclass_of($user_class, 'SocialLoginInterface')) {
			throw new \RuntimeException("Class $user_class must implement interface SocialLoginInterface");
		}

		self::$class = $user_class;
	}

}
