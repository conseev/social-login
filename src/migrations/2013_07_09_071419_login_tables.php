<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LoginTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('social_login_identities', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('global_id',50)->unique();
			$table->integer('user_id')->index();
			$table->string('type', 20);
			$table->string('email', 200)->index();
			$table->string('first_name', 150);
			$table->string('last_name', 150);
			$table->string('avatar', 250);
			$table->text('token');
			$table->timestamp('token_expires_at');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('social_login_identities');
	}

}
