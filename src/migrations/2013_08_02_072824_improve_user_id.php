<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImproveUserId extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('social_login_identities', function(Blueprint $table)
		{
			//
			$table->dropColumn('user_id');
		});

		Schema::table('social_login_identities', function(Blueprint $table)
		{
			//
			$table->string('user_id', 80)
				->after('global_id')
				->index();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('social_login_identities', function(Blueprint $table)
		{
			//
		});
	}

}
