<?php

namespace Conseev\SocialLogin;

use Illuminate\Support\ServiceProvider as Service;
use Config, URL, Route, SocialLoginIdentity, SocialLogin;

class ServiceProvider extends Service {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		require __DIR__ . "/../../routes.php";
		$services = Config::get('social_login');
		if (empty($services)) {
			throw new \RuntimeException("You must create a 'social_login' configuration in your app");
		}

		$user_class = "User";
		if (!empty($services['user_class'])) {
			$user_class =  $services['user_class'];
		}

		SocialLogin::setUserClass($user_class);

		$multipass = \MultiPass\Configuration::getInstance();
		foreach ($services as $name => $config) {
			if (!is_array($config)) continue;
			$config['callback_path'] = URL::route('sl_redirect_uri', array('type' => $name), false);
			$multipass->registerConfig($name, $config);
		}
		$multipass->register();
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}

