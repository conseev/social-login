<?php

namespace Conseev\SocialLogin;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Config, Redirect;
use SocialLoginIdentity;

class Controller extends \Controller
{
	public function doLogin($type)
	{
		$multipass = \MultiPass\Configuration::getInstance();
		$strategy  = $multipass->getStrategy(ucfirst($type));
		$strategy->requestPhase();
		exit;
	}

	public function doAuthorize($type)
	{
		$multipass = \MultiPass\Configuration::getInstance();
		$strategy = $multipass->getStrategy($type);
		$authHash = $strategy->callbackPhase();

		$guid = $type . ':' . $authHash->uid;
		$user = SocialLoginIdentity::where('global_id', $guid)
			->first();

		// Get all users by email
		if (!empty($user)) {
			$user->token = $authHash->credentials['token'];
			$user->token_expires_at = date("Y-m-d H:i:s", $authHash->credentials['expires_at']);
			$user->save();
			try {
				if (!$user->system_user) {
					throw new \Exception;
				}
				$user->system_user->doUpdate($authHash->info);
				$redirect = $user->login($type);
				return $redirect ?: Redirect::intended("/")->with('logged_in', $user);
			} catch (\Exception $e) {
				// We have an user record but the User
				// is missing
				$user->delete();
			}
		}

		$info = $authHash->info;
		$user = SocialLoginIdentity::register($info);

		$redirect = SocialLoginIdentity::create(array(
			'global_id' => $guid,
			'user_id'	=> $user->id,
			'type'      => strtolower($type),
			'email'     => $info['email'],
			'first_name' => $info['first_name'],
			'last_name' => $info['last_name'],
			'avatar'    => $info['image'],
			'token'     => $authHash->credentials['token'],
			'token_expires_at' => date("Y-m-d H:i:s", $authHash->credentials['expires_at']),
		))->login($type);

		return $redirect ?: Redirect::intended("/")->with('logged_in', $user);
	}
}

