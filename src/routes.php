<?php

$class = 'Conseev\SocialLogin\Controller';

Route::get('social-login/{type}', array('as' => 'sl_login', 'uses' => "$class@doLogin"));
Route::get('social-login/callback/{type}', array('as' => 'sl_redirect_uri', 'uses' => "$class@doAuthorize"));
