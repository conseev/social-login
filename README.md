Social Login
============

It is a laravel4 plugin that enables login/authentication/registration with Facebook, Google, Twitter and other accounts support OAuth/OAuth2.

The library works with the User model from your project, which must implment an interface.

How to setup
------------

We need to add the repository throught composer

```json
{
	"repositories": [
		{
			"type": "vcs",
			"url":  "git@bitbucket.org:conseev/social-login.git"
		}
	],
	"require": {
		"conseev/social-login": "dev-master"
	}
}
```

After running `composer update` we must edit `app/config/app.php` and append `'Conseev\SocialLogin\ServiceProvider'` in the `$providers` array.

Once it is installed we must create the needed tables

```bash
php artisan migrate --package="conseev/social-login"
```

Now the package is installed, and we need to configure. The configurations are saved in `app/config/social_login.php` and they contain information about the social network that we want to login with.

```php
<?php
return array(
	'Facebook' => [
		'client_id' => 'xxx',
		'client_secret' => 'xxx',
		'authorize_options' => [
			'scope' => 'email,user_about_me,user_location,user_website,offline_access'
		]
	],  
	'Google' => [
		'client_id' => 'xxx',
		'client_secret' => 'xxx',
	],
	// We can also use a different user Model
	// by default it would be User
	'user_class' => 'User',
);
```

Now we must extend our User model, and it must implement the `SocialLogin` interface. it should look more or less like this:


```php
<?php

class User extends Eloquent implements SocialLoginInterface
{
	public static function doLogin($user)
	{
		return Auth::login($user);
	}

	public static function doRegiter(Array $user)
	{
		return self::create([
			'email' => $user['email'],
			'name'  => $user['name'],
			'confirmed' => 1,
		]);
	}
}
```

NOTE: The model *must* have an unique field named `email`.

The library would call `doRegister` once, for instance if a given email is already an user, it would just start a new session.

Advanced operations
-------------------

The function `SocialLogin::getServices()` returns the available social logins

If a given user has a session with SocialLogin, it is possible for the application to get an OAuth2 or OAuth client, so they can perform further operations with their API providers.

```php
try {
	$google = SocialLoginIdentity::getIdentity('google');
	$url = 'https://www.googleapis.com/oauth2/v1/userinfo';
	$google->get($url, ['parse'=>'json'])->parse();
} catch (\Exception $e) {
}
```

If the API provider has a decent API client, we would instanciate that instead. For instance for Facebook (I'm looking for more clients) it would be like this:


```php
try {
	$fb = SocialLoginIdentity::getIdentity('facebook');
	var_dump($fb instanceof \Facebook); // true
	$data = $fb->api('/me');
} catch (\Exception $e) {
}
```
To read more about the `Facebook SDK` read this link https://developers.facebook.com/docs/reference/php/

If you want to get all the "social accounts" that a given user has you can call to `SocialLoginIdentity::getIdentities($user_object)`.
